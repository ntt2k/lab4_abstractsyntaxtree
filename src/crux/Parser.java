package crux;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import ast.Command;

public class Parser {
    public static String studentName = "Trung Nguyen";
    public static String studentID = "14853167";
    public static String uciNetID = "trungtn";


// SymbolTable Management ==========================
    private SymbolTable symbolTable;
    
    private void initSymbolTable()
    {
//        throw new RuntimeException("implement this");
        symbolTable = new SymbolTable();

    }
    
    private void enterScope()
    {
//        throw new RuntimeException("implement this");
        symbolTable = new SymbolTable(symbolTable);
    }

    private void exitScope()
    {
//        throw new RuntimeException("implement this");
        symbolTable = symbolTable.getParentTable();
    }

    private Symbol tryResolveSymbol(Token ident)
    {
        assert(ident.is(Token.Kind.IDENTIFIER));
        String name = ident.lexeme();
        try {
            return symbolTable.lookup(name);
        } catch (SymbolNotFoundError e) {
            String message = reportResolveSymbolError(name, ident.lineNumber(), ident.charPosition());
            return new ErrorSymbol(message);
        }
    }

    private String reportResolveSymbolError(String name, int lineNum, int charPos)
    {
        String message = "ResolveSymbolError(" + lineNum + "," + charPos + ")[Could not find " + name + ".]";
        errorBuffer.append(message + "\n");
        errorBuffer.append(symbolTable.toString() + "\n");
        return message;
    }

    private Symbol tryDeclareSymbol(Token ident)
    {
        assert(ident.is(Token.Kind.IDENTIFIER));
        String name = ident.lexeme();
        try {
            return symbolTable.insert(name);
        } catch (RedeclarationError re) {
            String message = reportDeclareSymbolError(name, ident.lineNumber(), ident.charPosition());
            return new ErrorSymbol(message);
        }
    }

    private String reportDeclareSymbolError(String name, int lineNum, int charPos)
    {
        String message = "DeclareSymbolError(" + lineNum + "," + charPos + ")[" + name + " already exists.]";
        errorBuffer.append(message + "\n");
        errorBuffer.append(symbolTable.toString() + "\n");
        return message;
    }    




// Error Reporting ==========================================

    private StringBuffer errorBuffer = new StringBuffer();

    private String reportSyntaxError(NonTerminal nt)
    {
        String message = "SyntaxError(" + lineNumber() + "," + charPosition() + ")[Expected a token from " + nt.name() + " but got " + currentToken.kind() + ".]";
        errorBuffer.append(message + "\n");
        return message;
    }

    private String reportSyntaxError(Token.Kind kind)
    {
        String message = "SyntaxError(" + lineNumber() + "," + charPosition() + ")[Expected " + kind + " but got " + currentToken.kind() + ".]";
        errorBuffer.append(message + "\n");
        return message;
    }

    public String errorReport()
    {
        return errorBuffer.toString();
    }

    public boolean hasError()
    {
        return errorBuffer.length() != 0;
    }

    private class QuitParseException extends RuntimeException
    {
        private static final long serialVersionUID = 1L;
        public QuitParseException(String errorMessage) {
            super(errorMessage);
        }
    }

    private int lineNumber()
    {
        return currentToken.lineNumber();
    }

    private int charPosition()
    {
        return currentToken.charPosition();
    }

    
// Parser ==========================================
    private Scanner scanner;
    private Token currentToken;

    public Parser(Scanner scanner)
    {
        this.scanner = scanner;
        this.currentToken = scanner.next();
    }
   
    public ast.Command parse()
    {
        initSymbolTable();
        try {
            return program();
        } catch (QuitParseException q) {
            return new ast.Error(lineNumber(), charPosition(), "Could not complete parsing.");
        }
    }
    
// Helper Methods ==========================================
    private boolean have(Token.Kind kind)
    {
        return currentToken.is(kind);
    }
    
    private boolean have(NonTerminal nt)
    {
        return nt.firstSet().contains(currentToken.kind());
    }
    
    private boolean accept(Token.Kind kind)
    {
        if (have(kind)) {
            currentToken = scanner.next();
            return true;
        }
        return false;
    }    
    
    private boolean accept(NonTerminal nt)
    {
        if (have(nt)) {
            currentToken = scanner.next();
            return true;
        }
        return false;
    }

    private boolean expect(Token.Kind kind)
    {
        if (accept(kind))
            return true;
        String errormessage = reportSyntaxError(kind);
        throw new QuitParseException(errormessage);
        //return false;
    }
        
    private boolean expect(NonTerminal nt)
    {
        if (accept(nt))
            return true;
        String errormessage = reportSyntaxError(nt);
        throw new QuitParseException(errormessage);
        //return false;
    }
     
    private Token expectRetrieve(Token.Kind kind)
    {
        Token tok = currentToken;
        if (accept(kind))
            return tok;
        String errorMessage = reportSyntaxError(kind);
        throw new QuitParseException(errorMessage);
        //return ErrorToken(errorMessage);
    }
        
    private Token expectRetrieve(NonTerminal nt)
    {
        Token tok = currentToken;
        if (accept(nt))
            return tok;
        String errorMessage = reportSyntaxError(nt);
        throw new QuitParseException(errorMessage);
        //return ErrorToken(errorMessage);
    }
   
// Grammar Rules =====================================================
    
    // literal := INTEGER | FLOAT | TRUE | FALSE .
    public ast.Expression literal()
    {
        ast.Expression expr;
//        enterRule(NonTerminal.LITERAL);
        
        Token tok = expectRetrieve(NonTerminal.LITERAL);
        expr = Command.newLiteral(tok);
        
//        exitRule(NonTerminal.LITERAL);
        return expr;
    }

    // designator := IDENTIFIER { "[" expression0 "]" } .
    public ast.Expression designator()
    {
        // enterRule(NonTerminal.DESIGNATOR);
        Token start = expectRetrieve(Token.Kind.IDENTIFIER);
        Symbol symbol = tryResolveSymbol(start);

        ast.Expression base = new ast.AddressOf(start.lineNumber(), start.charPosition(), symbol);
        while (accept(Token.Kind.OPEN_BRACKET)) {
            base = new ast.Index(currentToken.lineNumber(), currentToken.charPosition(), base, expression0());
            expect(Token.Kind.CLOSE_BRACKET);
        }

        // exitRule(NonTerminal.DESIGNATOR);
        return base;
    }

    // type := IDENTIFIER
    public void type()
    {
        // enterRule(NonTerminal.TYPE);
        expectRetrieve(Token.Kind.IDENTIFIER);
        // exitRule(NonTerminal.TYPE);
    }

    // op0 := ">=" | "<=" | "!=" | "==" | ">" | "<"
    public Token op0()
    {
        // enterRule(NonTerminal.OP0);

//        if (accept(Token.Kind.GREATER_EQUAL) || accept(Token.Kind.LESSER_EQUAL)
//                || accept(Token.Kind.NOT_EQUAL) || accept(Token.Kind.EQUAL)
//                || accept(Token.Kind.GREATER_THAN) || accept(Token.Kind.LESS_THAN)) {}
//
//        else {
//            String errorMessage = reportSyntaxError(NonTerminal.OP0);
//            throw new QuitParseException(errorMessage);
//        }

        // exitRule(NonTerminal.OP0);
        return expectRetrieve(NonTerminal.OP0);
    }

    // op1 := "+" | "-" | "or"
    public Token op1()
    {
        // enterRule(NonTerminal.OP1);

//        if (accept(Token.Kind.ADD) || accept(Token.Kind.SUB) || accept(Token.Kind.OR)) {}
//
//        else {
//            String errorMessage = reportSyntaxError(NonTerminal.OP1);
//            throw new QuitParseException(errorMessage);
//        }

        // exitRule(NonTerminal.OP1);
        return expectRetrieve(NonTerminal.OP1);
    }

    //op2 := "*" | "/" | "and"
    public Token op2()
    {
        // enterRule(NonTerminal.OP2);

//        if (accept(Token.Kind.MUL) || accept(Token.Kind.DIV) || accept(Token.Kind.AND)) {}
//
//        else {
//            String errorMessage = reportSyntaxError(NonTerminal.OP2);
//            throw new QuitParseException(errorMessage);
//        }

        // exitRule(NonTerminal.OP2);
        return expectRetrieve(NonTerminal.OP2);
    }

    // expression0 := expression1 [ op0 expression1 ]
    public ast.Expression expression0()
    {
        // enterRule(NonTerminal.EXPRESSION0);

        ast.Expression expression =  expression1();
        if (have(NonTerminal.OP0)) {
//            op0();
            expression = ast.Command.newExpression(expression, op0(), expression1());
        }

        // exitRule(NonTerminal.EXPRESSION0);
        return expression;
    }

    // expression1 := expression2 { op1  expression2 }
    public ast.Expression expression1()
    {
        // enterRule(NonTerminal.EXPRESSION1);

        ast.Expression expression = expression2();
        while(have(NonTerminal.OP1)) {
//            op1();
            expression = ast.Command.newExpression(expression, op1(), expression2());
        }

        // exitRule(NonTerminal.EXPRESSION1);
        return expression;
    }

    // expression2 := expression3 { op2 expression3 }
    public ast.Expression expression2()
    {
        // enterRule(NonTerminal.EXPRESSION2);

        ast.Expression expression = expression3();
        while(have(NonTerminal.OP2)) {
//            op2();
            expression = ast.Command.newExpression(expression, op2(), expression3());
        }

        // exitRule(NonTerminal.EXPRESSION2);
        return expression;
    }


//    expression3 := "not" expression3
//                   | "(" expression0 ")"
//                   | designator
//                   | call-expression
//                   | literal

    public ast.Expression expression3()
    {
        // enterRule(NonTerminal.EXPRESSION3);
        ast.Expression expr;

        if (have(Token.Kind.NOT)) {
            Token not = expectRetrieve(Token.Kind.NOT);
            expr = new ast.LogicalNot(not.lineNumber(), not.charPosition(), expression3());
        }

        else if (accept(Token.Kind.OPEN_PAREN))
        {
            expr = expression0();
            expect(Token.Kind.CLOSE_PAREN);
        }
        else if (have(NonTerminal.DESIGNATOR))
            // check the position of the Token ??
            // check the designator() ??
            expr = new ast.Dereference(currentToken.lineNumber(), currentToken.charPosition(), designator());

        else if (have(NonTerminal.CALL_EXPRESSION))
            expr = call_expression();

        else if (have(NonTerminal.LITERAL))
            expr = literal();

        else {
            String errorMessage = reportSyntaxError(NonTerminal.EXPRESSION3);
            expr = new ast.Error(currentToken.lineNumber(), currentToken.charPosition(), errorMessage);
        }

        // exitRule(NonTerminal.EXPRESSION3);
        return expr;
    }

    // call-expression := "::" IDENTIFIER "(" expression-list ")"
    public ast.Call call_expression()
    {
        // enterRule(NonTerminal.CALL_EXPRESSION);

        Token start = expectRetrieve(Token.Kind.CALL);
        Symbol sym = tryResolveSymbol(expectRetrieve(Token.Kind.IDENTIFIER));
        expect(Token.Kind.OPEN_PAREN);
        ast.ExpressionList expression_list = expression_list();
        expect(Token.Kind.CLOSE_PAREN);

        // exitRule(NonTerminal.CALL_EXPRESSION);
        return new ast.Call(start.lineNumber(), start.charPosition(), sym, expression_list);
    }

    // expression-list := [ expression0 { "," expression0 } ]
    public ast.ExpressionList expression_list()
    {
        // enterRule(NonTerminal.EXPRESSION_LIST);
        ast.ExpressionList expressionList = new ast.ExpressionList(currentToken.lineNumber(), currentToken.charPosition());

        if (have(NonTerminal.EXPRESSION0)) {
            expressionList.add(expression0());
            while (accept(Token.Kind.COMMA))
                expressionList.add(expression0());
        }

        // exitRule(NonTerminal.EXPRESSION_LIST);
        return expressionList;
    }

    // parameter := IDENTIFIER ":" type
    public Symbol parameter()
    {
        // enterRule(NonTerminal.PARAMETER);

        Symbol symbol = tryDeclareSymbol(expectRetrieve(Token.Kind.IDENTIFIER));
        expect(Token.Kind.COLON);
        type();

        // exitRule(NonTerminal.PARAMETER);
        return symbol;
    }

    // parameter-list := [ parameter { "," parameter } ]
    public List<Symbol> parameter_list()
    {
        // enterRule(NonTerminal.PARAMETER_LIST);
        List<Symbol> objlist = new ArrayList<Symbol>();

        if (have(NonTerminal.PARAMETER)) {
            objlist.add(parameter());
            while (accept(Token.Kind.COMMA))
                objlist.add(parameter());
        }

        // exitRule(NonTerminal.PARAMETER_LIST);
        return objlist;

    }

    // variable-declaration := "var" IDENTIFIER ":" type ";"
    public ast.VariableDeclaration variable_declaration()
    {
        // enterRule(NonTerminal.VARIABLE_DECLARATION);

        Token start = expectRetrieve(Token.Kind.VAR);
        Symbol symbol = tryDeclareSymbol(expectRetrieve(Token.Kind.IDENTIFIER));

        expect(Token.Kind.COLON);
        type();
        expect(Token.Kind.SEMICOLON);

        // exitRule(NonTerminal.VARIABLE_DECLARATION);
        return new ast.VariableDeclaration(start.lineNumber(), start.charPosition(), symbol);
    }

    // array-declaration := "array" IDENTIFIER ":" type "[" INTEGER "]" { "[" INTEGER "]" } ";"
    public ast.ArrayDeclaration array_declaration()
    {
        // enterRule(NonTerminal.ARRAY_DECLARATION);

        Token start = expectRetrieve(Token.Kind.ARRAY);
        Symbol symbol = tryDeclareSymbol(expectRetrieve(Token.Kind.IDENTIFIER));

        expect(Token.Kind.COLON);
        type();
        expect(Token.Kind.OPEN_BRACKET);
        expect(Token.Kind.INTEGER);
        expect(Token.Kind.CLOSE_BRACKET);
        while (accept(Token.Kind.OPEN_BRACKET)) {
            expect(Token.Kind.INTEGER);
            expect(Token.Kind.CLOSE_BRACKET);
        }
        expect(Token.Kind.SEMICOLON);

        // exitRule(NonTerminal.ARRAY_DECLARATION);
        return new ast.ArrayDeclaration(start.lineNumber(), start.charPosition(), symbol);
    }

    // function-definition := "func" IDENTIFIER "(" parameter-list ")" ":" type statement-block
    public ast.FunctionDefinition function_definition()
    {
        // enterRule(NonTerminal.FUNCTION_DEFINITION);

        Token start = expectRetrieve(Token.Kind.FUNC);
        Symbol symbol = tryDeclareSymbol(expectRetrieve(Token.Kind.IDENTIFIER));

        expect(Token.Kind.OPEN_PAREN);
        enterScope();

        List<Symbol> paralist = parameter_list();
        expect(Token.Kind.CLOSE_PAREN);
        expect(Token.Kind.COLON);
        type();

        ast.StatementList statementlist = statement_block();
        exitScope();

        // exitRule(NonTerminal.FUNCTION_DEFINITION);
        return new ast.FunctionDefinition(start.lineNumber(), start.charPosition(), symbol, paralist, statementlist);
    }

    // declaration := variable-declaration | array-declaration | function-definition
    public ast.Declaration declaration()
    {
        // enterRule(NonTerminal.DECLARATION);
        ast.Declaration obj;

        if (have(NonTerminal.VARIABLE_DECLARATION))
            obj = variable_declaration();
        else if (have(NonTerminal.ARRAY_DECLARATION))
            obj = array_declaration();
        else if (have(NonTerminal.FUNCTION_DEFINITION))
            obj = function_definition();
        else {
            String errorMessage = reportSyntaxError(NonTerminal.DECLARATION);
            throw new QuitParseException(errorMessage);
        }

        // exitRule(NonTerminal.DECLARATION);
        return obj;
    }

    // declaration-list := { declaration }
    public ast.DeclarationList declaration_list()
    {
        // enterRule(NonTerminal.DECLARATION_LIST);
        ast.DeclarationList objList = new ast.DeclarationList(currentToken.lineNumber(),currentToken.charPosition());
        while (have(NonTerminal.DECLARATION))  {
            objList.add(declaration());
        }
        // exitRule(NonTerminal.DECLARATION_LIST);
        return objList;

    }

    // assignment-statement := "let" designator "=" expression0 ";"
    public ast.Assignment assignment_statement()
    {
        // enterRule(NonTerminal.ASSIGNMENT_STATEMENT);

        Token start = expectRetrieve(Token.Kind.LET);               // START HERE OR THE ASSIGN ??
        ast.Expression designator = designator();
        expect(Token.Kind.ASSIGN);
        ast.Expression expression0 =  expression0();
        expect(Token.Kind.SEMICOLON);

        // exitRule(NonTerminal.ASSIGNMENT_STATEMENT);
        return new ast.Assignment(start.lineNumber(), start.charPosition(), designator, expression0);
    }

    // call-statement := call-expression ";"
    public ast.Call call_statement()
    {
        // enterRule(NonTerminal.CALL_STATEMENT);

        ast.Call call_statement = call_expression();
        expect(Token.Kind.SEMICOLON);

        // exitRule(NonTerminal.CALL_STATEMENT);
        return call_statement;
    }

    // if-statement := "if" expression0 statement-block [ "else" statement-block ]
    public ast.IfElseBranch if_statement()
    {
        // enterRule(NonTerminal.IF_STATEMENT);

        Token start = expectRetrieve(Token.Kind.IF);
        ast.Expression expression = expression0();
        enterScope();
        ast.StatementList firstBlock = statement_block();
        exitScope();

        ast.StatementList secondBlock = new ast.StatementList(currentToken.lineNumber(), currentToken.charPosition());
        if (accept(Token.Kind.ELSE)){
            enterScope();
            secondBlock = statement_block();
            exitScope();
        }
        // exitRule(NonTerminal.IF_STATEMENT);
        return new ast.IfElseBranch(start.lineNumber(), start.charPosition(), expression, firstBlock, secondBlock);
    }

    // while-statement := "while" expression0 statement-block
    public ast.WhileLoop while_statement()
    {
        // enterRule(NonTerminal.WHILE_STATEMENT);

        Token start = expectRetrieve(Token.Kind.WHILE);
        ast.Expression expression = expression0();
        enterScope();
        ast.StatementList statementBlock = statement_block();
        exitScope();

        // exitRule(NonTerminal.WHILE_STATEMENT);
        return new ast.WhileLoop(start.lineNumber(), start.charPosition(), expression, statementBlock);
    }

    // return-statement := "return" expression0 ";"
    public ast.Return return_statement()
    {
        // enterRule(NonTerminal.RETURN_STATEMENT);

        Token start = expectRetrieve(Token.Kind.RETURN);
        ast.Expression expression = expression0();
        expect(Token.Kind.SEMICOLON);

        // exitRule(NonTerminal.RETURN_STATEMENT);
        return new ast.Return(start.lineNumber(), start.charPosition(), expression);
    }

//    statement := variable-declaration
//                | call-statement
//                | assignment-statement
//                | if-statement
//                | while-statement
//                | return-statement

    public ast.Statement statement()
    {
        // enterRule(NonTerminal.STATEMENT);
        ast.Statement sta;

        if (have(NonTerminal.VARIABLE_DECLARATION))
            sta = variable_declaration();
        else if (have(NonTerminal.CALL_STATEMENT))
            sta = call_statement();
        else if (have(NonTerminal.ASSIGNMENT_STATEMENT))
            sta = assignment_statement();
        else if (have(NonTerminal.IF_STATEMENT))
            sta = if_statement();
        else if (have(NonTerminal.WHILE_STATEMENT))
            sta = while_statement();
        else if (have(NonTerminal.RETURN_STATEMENT))
            sta = return_statement();
        else {
            String errorMessage = reportSyntaxError(NonTerminal.STATEMENT);
            throw new QuitParseException(errorMessage);
        }

        // exitRule(NonTerminal.STATEMENT);
        return sta;
    }

    // statement-list := { statement }
    public ast.StatementList statement_list()
    {
        // enterRule(NonTerminal.STATEMENT_LIST);
        ast.StatementList stamenList = new ast.StatementList(currentToken.lineNumber(), currentToken.charPosition());

        while (have(NonTerminal.STATEMENT))
            stamenList.add(statement());

        // exitRule(NonTerminal.STATEMENT_LIST);
        return stamenList;
    }

    // statement-block := "{" statement-list "}"
    public ast.StatementList statement_block()
    {
        // enterRule(NonTerminal.STATEMENT_BLOCK);

        expect(Token.Kind.OPEN_BRACE);
        ast.StatementList statementList = statement_list();
        expect(Token.Kind.CLOSE_BRACE);

        // exitRule(NonTerminal.STATEMENT_BLOCK);
        return statementList;
    }



    // program := declaration-list EOF .
    public ast.DeclarationList program()
    {
        ast.DeclarationList result = declaration_list();
        expect(Token.Kind.EOF);
        return result;

    }
    
}
