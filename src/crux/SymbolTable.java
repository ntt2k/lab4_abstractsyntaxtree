package crux;

import java.util.LinkedHashMap;

public class SymbolTable {
    private LinkedHashMap<String, Symbol> symboltable;
    private SymbolTable parent;
    int depth;
    
    public SymbolTable()
    {
//        throw new RuntimeException("implement this");
        symboltable = new LinkedHashMap<String, Symbol>();
        parent = null;
        depth = 0;

        // initialize
        symboltable.put("readInt", new Symbol("readInt"));
        symboltable.put("readFloat", new Symbol("readFloat"));
        symboltable.put("printBool", new Symbol("printBool"));
        symboltable.put("printInt", new Symbol("printInt"));
        symboltable.put("printFloat", new Symbol("printFloat"));
        symboltable.put("println", new Symbol("println"));

    }

    public SymbolTable(SymbolTable table)
    {
//        throw new RuntimeException("implement this");
        symboltable = new LinkedHashMap<String, Symbol>();
        parent = table;
        depth = table.depth + 1;

    }
    
    public Symbol lookup(String name) throws SymbolNotFoundError
    {
//        throw new RuntimeException("implement this");
        if (symboltable.containsKey(name)) {
            return symboltable.get(name);
        }

        else if (depth == 0 && symboltable.get(name) == null) {
            throw new SymbolNotFoundError(name);
        }

        else {
            return parent.lookup(name);
        }
    }
       
    public Symbol insert(String name) throws RedeclarationError
    {
//        throw new RuntimeException("implement this");
        Symbol newSymbol = symboltable.get(name);

        if (newSymbol == null) {

            newSymbol = new Symbol(name);
            symboltable.put(name, newSymbol);

            return newSymbol;
        }
        else {
            throw new RedeclarationError(newSymbol);
        }

    }
    
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        if (parent != null) /*I have a parent table*/
            sb.append(parent.toString());
        
        String indent = new String();
        for (int i = 0; i < depth; i++) {
            indent += "  ";
        }
        
        for (Symbol s: symboltable.values()) /*Every symbol, s, in this table*/
        {
            sb.append(indent + s.toString() + "\n");
        }
        return sb.toString();
    }

    public SymbolTable getParentTable() {
        return parent;
    }
}

class SymbolNotFoundError extends Error
{
    private static final long serialVersionUID = 1L;
    private String name;
    
    SymbolNotFoundError(String name)
    {
        this.name = name;
    }
    
    public String name()
    {
        return name;
    }
}

class RedeclarationError extends Error
{
    private static final long serialVersionUID = 1L;

    public RedeclarationError(Symbol sym)
    {
        super("Symbol " + sym + " being redeclared.");
    }
}
